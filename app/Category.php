<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    use \Dimsav\Translatable\Translatable;
    
    public $translatedAttributes = ['name'];
    protected $guarded = [];
}
