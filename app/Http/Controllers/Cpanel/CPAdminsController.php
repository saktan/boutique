<?php

namespace App\Http\Controllers\Cpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\User;
use Auth;
use Image; 
use Storage;

class CPAdminsController extends Controller
{
    public function __construct(){
        $this->middleware(['permission:create_users'])->only(['create','store']);
        $this->middleware(['permission:update_users'])->only(['edit','update']);
        $this->middleware(['permission:read_users'])->only('index');
        $this->middleware(['permission:delete_users'])->only('destroy');
    }

    public function index(Request $request)
    {
       /*
            search without checking if role is admin
        if($request->search){
            $users = User::where('first_name','like','%'.$request->search.'%')
            ->orWhere('last_name','like','%'.$request->search.'%')
            ->orWhere('email','like','%'.$request->search.'%')
            ->get();
        }else{
            $users = User::whereRoleIs('admin')->get();
        }
        
        */
        
        if($request->ajax()){
            return DataTables(User::WhereRoleIs('admin'))
            ->addColumn('actions',"cpanel.admins.dt_cols.actions")
            ->addColumn('user_image',"cpanel.admins.dt_cols.user_image")
            ->rawColumns(['actions','user_image'])
            ->toJson();
        }
        
        $users = User::WhereRoleIs('admin')->where(function($e) use ($request){
            // rules after where role is admin
            return $e->when($request->search,function($query) use ($request) {
                // rules after when request search
                return $query->where('first_name','like','%'.$request->search.'%')
                ->orWhere('last_name','like','%'.$request->search.'%')
                ->orWhere('email','like','%'.$request->search.'%');
            });
        })->get();

        return view('cpanel.admins.index');
    }

    public function create()
    {
        return view('cpanel.admins.create');
    }


    public function store(Request $request)
    {        
        $request->validate([
            'first_name' => 'bail|required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'permissions' => 'required',
            'photo' => 'image'
        ]);

        $request_data = $request->except(['password','password_confirmation','permissions','photo']);
        $request_data['password'] = bcrypt($request->password);

        // Photo 

        if($request->photo){

            $img = Image::make($request->photo);
            
            $img->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('uploads/users_images/'.$request->photo->hashName()));

            $request_data['photo'] = $request->photo->hashName();
        }

        $user = User::create($request_data);
        $user->attachRole('admin');
        $user->attachPermissions($request->permissions);
        return redirect(route('admins.index'))->with('success',__('site.admin_added_success'));
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('cpanel.admins.edit',compact('user'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => ['required','string','email',Rule::unique('users')->ignore($id)],
            'photo' => 'image',
            'permissions' => 'required'
        ]);

        $user = User::find($id);

        $request_data = $request->except(['password','password_confirmation','permissions','photo']);

        if($request->photo){

            if($user->photo != 'default.png'){
                Storage::disk('public_uploads')->delete('/users_images/'.$user->photo);
            }

            $img = Image::make($request->photo);
            
            $img->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('uploads/users_images/'.$request->photo->hashName()));

            $request_data['photo'] = $request->photo->hashName();
        }

        $user->update($request_data);
        //detach old permissions 
        $user->detachPermissions($user->permissions);
        // attach new permissions 
        $user->attachPermissions($request->permissions);
        
        return redirect( route('admins.index') )->with('success',__('site.admin_updated_success'));
    }

    public function destroy($id)
    {
        if(Auth::id() == $id){
            return redirect( route('admins.index') )->with('error',__('site.error_self_delete'));
        }

        
        $user = User::find($id);

        // delete user image
        if($user->photo != 'default.png'){
            Storage::disk('public_uploads')->delete('/users_images/'.$user->photo);
        }

        if($user->hasRole('super_admin')){
            return redirect( route('admins.index') )->with('error',__('site.error_super_admin_delete'));
        }
        if($user->delete()){
            return redirect( route('admins.index') )->with('success',__('site.delete_success',[ 'name' => $user->first_name.' '.$user->last_name ]));
        }
    }
}
