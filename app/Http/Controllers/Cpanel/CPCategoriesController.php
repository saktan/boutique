<?php
namespace App\Http\Controllers\Cpanel;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class CPCategoriesController extends Controller
{
    public function __construct(){
        $this->middleware(['permission:create_categories'])->only(['create','store']);
        $this->middleware(['permission:update_categories'])->only(['edit','update']);
        $this->middleware(['permission:read_categories'])->only('index');
        $this->middleware(['permission:delete_categories'])->only('destroy');
    }
    public function index(Request $request)
    {
        if($request->ajax()){
            return DataTables(Category::all())
            ->addColumn('actions','cpanel.categories.dt_cols.actions')
            ->rawColumns(['actions'])
            ->toJson();
        }
        return view('cpanel.categories.index');
    }

    public function create()
    {
        return view('cpanel.categories.create');
    }

    public function store(Request $request)
    {
        $locales = config('translatable.locales');
        $validation = [];

        foreach($locales as $locale){
            $validation[$locale.'.name'] = 'required|unique:category_translations,name';
        }

        $request->validate($validation);
        
        Category::Create($request->all());
        return redirect( route('categories.index') )->with(['success'=>'category created successfully !']);
        
    }

    public function edit(category $category)
    {
        return view('cpanel.categories.edit',compact('category'));
    }

    public function update(Request $request, category $category)
    {
        $locales = config('translatable.locales');
        $validation = [];
        
        foreach($locales as $locale){
            $validation[$locale.'.name'] = [Rule::unique('category_translations','name')->ignore($category->id, 'category_id'),'required'];
        }

        $request->validate($validation);

        $category->update($request->all());
        return redirect( route('categories.index') )->with(['success'=>'category updated successfully !']); 
    }

    public function destroy(category $category)
    {
        $category->delete();
        return redirect( route('categories.index') )->with(['success'=>'category deleted successfully !']); 
    }
}