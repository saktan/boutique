<?php

namespace App\Http\Controllers\Cpanel;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class CPProductsController extends Controller
{
    public function __construct(){
        $this->middleware(['permission:create_products'])->only(['create','store']);
        $this->middleware(['permission:update_products'])->only(['edit','update']);
        $this->middleware(['permission:read_products'])->only('index');
        $this->middleware(['permission:delete_products'])->only('destroy');
    }

    public function index(Request $request)
    {
        return view('cpanel.products.index');
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function edit(Product $product)
    {
        //
    }

    public function update(Request $request, Product $product)
    {
        //
    }

    public function destroy(Product $product)
    {
        //
    }
}
