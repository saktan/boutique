<?php

namespace App\Http\Middleware;

use Closure;

class roles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $roles = $request->route()->getAction('roles');
        return $next($request);
    }
}
