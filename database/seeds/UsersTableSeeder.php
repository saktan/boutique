<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'first_name' => 'super',
            'last_name' => 'admin',
            'email' => 'bedevelo.com@gmail.com',
            'password' => bcrypt('codeigniter'),
        ]);
        $this->command->info("Creating Default user");
        $user->attachRole('super_admin');
    }
}
