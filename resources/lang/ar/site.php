<?php

return [
    //dashboard

    'dashboard' => 'الرئيسة',
    //users
    'users' => 'المستخدمين',
    'admins' => 'المشرفين',
    'first_name' => 'الإسم الأول',
    'last_name' => 'الإسم الثاني',
    'email' => 'البريد الإلكتروني',
    'add_user' => 'اضافة مستخدم',
    'add_admin' => 'اضافة مشرف',
    'password' => 'كلمه السر',
    'confirm_password' => 'تأكيد كلمة المرور',
    'permissions' => 'الصلاحيات',
    'register' => 'تسجيل',
    'login' => 'تسجيل الدخول',
    'save' => 'حفظ',
    'save_changes' => 'حفظ التغييرات',
    'remember_me' => 'تذكرنى',
    'forgot_password' => 'نسيت رقمك السري ؟',
    'admin_added_success' => 'تمت إضافة المشرف بنجاح',
    'admin_updated_success' => 'تم تعديل المشرف بنجاح',
    'photo' => 'الصورة',

    //confirmation 

    'delete_confirm_alert' => 'هل ترغب حقا في حذف  ',
    'confirm_yes' => 'نعم',
    'confirm_no' => 'لا',

    //roles
    'admin' => 'مشرف',
    'user' => 'مستخدم',
    'super_admin' => 'مشرف سوبر',
    'roles' => 'الأدوار',

    //posts
    'posts' => 'المقالات',

    //data
    'no_data' => 'لايوجد بيانات',

    //actions
    'actions' => 'مهمات',
    'add' => 'أضف',
    'edit' => 'تعديل',
    'search' => 'بحث',

    //crud
    'create' => 'اضافة',
    'read' => 'قراءة',
    'update' => 'تعديل',
    'delete' => 'حذف',

    //delete
    'error_self_delete' => 'لا يمكنك حذف نفسك',
    'delete_success' => 'تم حذف :name بنجاح',
    'error_super_admin_delete' => 'لا يمكن حذف المشرف السوبر',

    // categories
    'categories' => 'الأقسام',
    'category' => 'قسم',
    'create_category' => 'إنشاء قسم',
    'edit_category' => 'تعديل قسم',
    'delete_category' => 'حذف قسم',
    

    // products 

    'products' => 'المنتوجات',
    'product' => 'منتوج',
    'add_product' => 'اضافة منتوج',
    'edit_product' => 'تعديل منتوج',
    'delete_product' => 'حذف منتوج',

    // Translatable

    'ar' => [
        'name' => 'الإسم باللغة العربية'
    ],

    'en' => [
        'name' => 'الإسم باللغة الإنجليزية'
    ]
];