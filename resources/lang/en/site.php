<?php

return [
    'dashboard' => 'Dashboard',
    'users' => 'Users',
    'first_name' => 'First name',
    'last_name' => 'Last name',
    'email' => 'E-Mail Address',
    'add_user' => 'Add User',
    'no_data' => 'there is no data',
    'actions' => 'Actions'
];