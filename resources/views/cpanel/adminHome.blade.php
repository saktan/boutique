{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Users')

@section('content_header')
    <h1>Home</h1>
    {{ Breadcrumbs::render('home') }}
@stop

@section('content')
@lang('site.dashboard')
@stop