{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title')
  @lang('site.add_admin')
@stop
@section('content_header')
    <h1>@lang('site.add_admin'):</h1>
    {{ Breadcrumbs::render('create_users') }}
@stop

@section('content')

<div class="row ">
    <div class="col-md-12">

                <div class="box box-info">

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">

                                <form action="{{ route('admins.store') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <!-- First Name -->
                                    <div class="form-group @error('first_name') has-error @enderror">
                                        <label>@lang('site.first_name'):</label>
                                        <input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}">
                                        @if($errors->has('first_name'))
                                            <span class="text text-danger">@error('first_name')
                                            {{ $message }}
                                            @enderror</span>
                                        @endif
                                    </div>

                                    <!-- Last Name -->
                                    <div class="form-group {{ $errors->has('last_name') ? 'has-error' : ''}}">
                                        <label>@lang('site.last_name'):</label>
                                        <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}">
                                        @if($errors->has('last_name'))
                                            <span class="text text-danger">{{ $errors->first('last_name') }}</span>
                                        @endif
                                    </div>

                                    <!-- Email -->
                                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                        <label>@lang('site.email'):</label>
                                        <input type="text" class="form-control " name="email" value="{{ old('email') }}">
                                        @if($errors->has('email'))
                                            <span class="text text-danger">{{ $errors->first('email') }}</span>
                                        @endif

                                    </div>   


                                    <!-- Photo -->
                                    <div class="form-group {{ $errors->has('photo') ? 'has-error' : '' }}">
                                        <label>@lang('site.photo'):</label>
                                        <input type="file" class="" name="photo" value="{{ old('photo') }}" />
                                        @if($errors->has('photo'))
                                            <span class="text text-danger">{{ $errors->first('photo') }}</span>
                                        @endif

                                    </div>
                                    <!-- Preview Photo -->
                                    <div class="form-group">
                                        <img src="{{ asset('uploads/users_images/default.png') }}" alt="" width="70" class="img img-responsive img-preview">
                                    </div>  

                                    <!-- Password -->
                                    <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                        <label>@lang('site.password'):</label>
                                        <input type="text" class="form-control" name="password">
                                        @if($errors->has('password'))
                                            <span class="text text-danger">{{ $errors->first('password') }}</span>
                                        @endif
                                    </div> 

                                    <!-- Confirm Password -->
                                    <div class="form-group">
                                        <label>@lang('site.confirm_password'):</label>
                                        <input type="text" class="form-control" name="password_confirmation">
                                    </div>   
                                    
                                    <!-- Permissions -->
                                    @php 
                                        //$models = ['users','categories','posts'];
                                        $models = config('laratrust_seeder.role_structure.super_admin');
                                        $map = config('laratrust_seeder.permissions_map');
                                        $i = 0;
                                
                                    @endphp

                                   <div class="form-group">
                                       <label for="">@lang('site.permissions'):</label>
                                       <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            @php $i = 0; @endphp
                                            @foreach($models as $key=>$value)
                                               
                                                <li class="{{ $i == 0 ? 'active' : '' }}"><a href="#{{ $key }}" data-toggle="tab">@lang('site.'.$key)</a></li>
                                                @php $i++ @endphp
                                            @endforeach
                                        </ul>

                                        <div class="tab-content">
                                            @php $i = 0; @endphp
                                            @foreach($models as $model=>$crud)
                                                <div class="tab-pane {{ $i == 0 ? 'active' : '' }}" id="{{ $model }}">

                                                    <div class="form-group">
                                                    @php $crud = explode(',',$crud) @endphp
                                                    @foreach($crud as $index=>$permission)
                                                      
                                                        @if(!empty($permission) && array_key_exists($permission, $map))
                                                            <label>
                                                                <input type="checkbox" {{ $index == 1 ? 'checked' : '' }} class="flat-red" name="permissions[]" value="{{ $map["$permission"].'_'.$model }}"> @lang('site.'.$map["$permission"]) 
                                                            </label>
                                                        @endif
                                                          
                                                    @endforeach
                                      
                                                    </div>

                                                </div>
                                                @php $i++ @endphp
                                            @endforeach
                        
                                            
                                            <!-- /.tab-pane -->
                                        </div>
                                        <!-- /.tab-content -->
                                    </div>
                                    @error('permissions')
                                      <span class="text text-danger">{{ $message }}</span>
                                    @enderror
                                   </div>
                                    <!-- Submit -->
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">@lang('site.add')</button>
                                    </div>                                
                                </form>

                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->
    </div>
</div>

@stop