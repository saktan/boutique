@if(auth::User()->can('update_users') )
<a class="btn btn-success btn-xs" href="{{ route('admins.edit',$id) }}"><i class="fa fa-edit"></i> @lang('site.edit')</a>
@else
<a class="btn btn-success disabled btn-xs" href="#"><i class="fa fa-edit"></i> @lang('site.edit')</a>
@endif

@if(auth::User()->can('delete_users') )
<button type="submit" class="btn btn-danger btn-xs delete" data-user_id="{{ $id }}" data-user_name="{{ $first_name.' '.$last_name }}"> <i class="fa fa-trash"></i>
@lang('site.delete')</button>
<form action="{{ route('admins.destroy',$id) }}" id="form_{{ $id }}" method="post" style="display:inline-block;">
    @csrf
    @method('delete')
</form>
@else
<a href="#" class="btn btn-danger disabled btn-xs"><i class="fa fa-trash"></i> @lang('site.delete')</a>
@endif