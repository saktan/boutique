{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title')
  @lang('site.add_admin')
@stop
@section('content_header')
    <h1>@lang('site.add_admin'):</h1>
    {{ Breadcrumbs::render('edit_users',$user) }}
@stop

@section('content')

<div class="row ">
    <div class="col-md-12">


                <div class="box box-info">

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">

                                <form action="{{ route('admins.update',$user->id) }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <!-- First Name -->
                                    <div class="form-group @error('first_name') has-error @enderror">
                                        <label>@lang('site.first_name'):</label>
                                        <input type="text" class="form-control" name="first_name" value="{{ $user->first_name }}">
                                        @if($errors->has('first_name'))
                                            <span class="text text-danger">@error('first_name')
                                            {{ $message }}
                                            @enderror</span>
                                        @endif
                                    </div>

                                    <!-- Last Name -->
                                    <div class="form-group {{ $errors->has('last_name') ? 'has-error' : ''}}">
                                        <label>@lang('site.last_name'):</label>
                                        <input type="text" class="form-control" name="last_name" value="{{ $user->last_name }}">
                                        @if($errors->has('last_name'))
                                            <span class="text text-danger">{{ $errors->first('last_name') }}</span>
                                        @endif
                                    </div>

                                    <!-- Email -->
                                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                        <label>@lang('site.email'):</label>
                                        <input type="text" class="form-control " name="email" value="{{ $user->email }}">
                                        @if($errors->has('email'))
                                            <span class="text text-danger">{{ $errors->first('email') }}</span>
                                        @endif

                                    </div> 

                                    <!-- Photo -->
                                    <div class="form-group {{ $errors->has('photo') ? 'has-error' : '' }}">
                                        <label>@lang('site.photo'):</label>
                                        <input type="file" class="" name="photo"/>
                                        @if($errors->has('photo'))
                                            <span class="text text-danger">{{ $errors->first('photo') }}</span>
                                        @endif

                                    </div>
                                    <!-- Preview Photo -->
                                    <div class="form-group">
                                        <img src="{{ $user->image_path }}" alt="" id="image-preview" width="70" class="img img-responsive img-preview">
                                    </div>  

                                    <!-- Permissions -->
                                    @php 
                                        //$models = ['users','categories','posts'];
                                        $models = config('laratrust_seeder.role_structure.super_admin');
                                        $map = config('laratrust_seeder.permissions_map');
                                        $i = 0;
                                
                                    @endphp

                                   <div class="form-group">
                                       <label for="">@lang('site.permissions'):</label>
                                       <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            @php $i = 0; @endphp
                                            @foreach($models as $key=>$value)
                                                <li class="{{ $i == 0 ? 'active' : '' }}"><a href="#{{ $key }}" data-toggle="tab">@lang('site.'.$key)</a></li>
                                                @php $i++ @endphp
                                            @endforeach
                                        </ul>

                                        <div class="tab-content">
                                            @php $i = 0; @endphp
                                            @foreach($models as $model=>$crud)

                                                <div class="tab-pane {{ $i == 0 ? 'active' : '' }}" id="{{ $model }}">

                                                    <div class="form-group">
                                                    @php $crud = explode(',',$crud) @endphp
                                                    @foreach($crud as $index=>$permission)
                                                    
                                                    <label>
                                                        <input type="checkbox" {{ $user->hasPermission($map["$permission"].'_'.$model) ? 'checked' : '' }} class="flat-red" name="permissions[]" value="{{ $map["$permission"].'_'.$model }}"> @lang('site.'.$map["$permission"]) 
                                                    </label>
                                                          
                                                    @endforeach
                                      
                                                    </div>

                                                </div>
                                                @php $i++ @endphp
                                            @endforeach
                        
                                            
                                            <!-- /.tab-pane -->
                                        </div>
                                        <!-- /.tab-content -->
                                    </div>
                                    @error('permissions')
                                      <span class="text text-danger">{{ $message }}</span>
                                    @enderror
                                   </div>
                                    <!-- Submit -->
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">@lang('site.update')</button>
                                    </div>                                
                                </form>

                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->
    </div>
</div>

@stop