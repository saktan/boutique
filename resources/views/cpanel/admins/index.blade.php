{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title')
    @lang('site.admins')
@stop

@section('content_header')
    <h1>@lang('site.admins')</h1>
    {{ Breadcrumbs::render('admins') }}
@stop

@section('content')

<div class="box box-primary">
            <div class="box-header">
            <div class="row">
                <div class="col-md-8">
                    <!-- Create Admin -->

                    @if(auth::User()->can('create_users'))
                        <a class="btn btn-primary" href="{{ route('admins.create') }}"><i class="fa fa-plus"></i> @lang('site.add_admin')</a>
                        @else
                        <a class="btn btn-primary disabled" href="#"><i class="fa fa-plus"></i> @lang('site.add_admin')</a>
                    @endif

                </div>

                <div class="col-md-4">
                    
                </div>
            </div>
            
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-responsive" id="datatable">
              <thead>
                    <tr>
                        <th>#</th>
                        <th>@lang('site.first_name')</th>
                        <th>@lang('site.last_name')</th>
                        <th>@lang('site.email')</th>
                        <th>@lang('site.photo')</th>
                        <th>@lang('site.roles')</th>
                        <th>@lang('site.actions')</th>
                    </tr>
              </thead>
               
              </table>
            </div>
            <!-- /.box-body -->
          </div>
@stop

@section('js')
<script>

        $('#datatable').DataTable({
            responsive : true,
            processing : true,
            ajax : '{{ route('admins.index') }}',
            columns : [
                {'data' : 'id'},
                {'data' : 'first_name'},
                {'data' : 'last_name'},
                {'data' : 'email'},
                {'data' : 'user_image'},
                {'data' : 'user_roles'},
                {'data' : 'actions'},
            ],

        });
    </script>   
@stop
