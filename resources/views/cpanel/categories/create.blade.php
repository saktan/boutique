{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title')
  @lang('site.create_category')
@stop
@section('content_header')
    <h1>@lang('site.create_category'):</h1>
    {{ Breadcrumbs::render('create_category') }}
@stop

@section('content')

<div class="row ">
    <div class="col-md-12">

                <div class="box box-info">

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">

                                <form action="{{ route('categories.store') }}" method="post">
                                    @csrf
                                    <!-- First Name -->

                                    @foreach(config('translatable.locales') as $local)

                                        <div class="form-group @error($local.'.name') has-error @enderror">
                                            <label>@lang('site.'.$local.'.name'):</label>
                                            <input type="text" class="form-control" name="{{ $local }}[name]" value="{{ old($local.'.name') }}">
                                            
                                                <span class="text text-danger">@error($local.'.name')
                                                {{ $message }}
                                                @enderror</span>
                                            
                                        </div>

                                    @endforeach


                                  <!--Submit-->
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">@lang('site.add')</button>
                                    </div>                                
                                </form>

                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->
    </div>
</div>

@stop