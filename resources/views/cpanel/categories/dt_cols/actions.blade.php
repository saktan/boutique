@if(auth::User()->can('update_categories') )
<a class="btn btn-success btn-xs" href="{{ route('categories.edit',$id) }}"><i class="fa fa-edit"></i> @lang('site.edit')</a>
@else
<a class="btn btn-success disabled btn-xs" href="#"><i class="fa fa-edit"></i> @lang('site.edit')</a>
@endif

@if(auth::User()->can('delete_categories') )
<button type="submit" class="btn btn-danger btn-xs delete" data-user_id="{{ $id }}" data-user_name="{{ $name }}"> <i class="fa fa-trash"></i>
@lang('site.delete')</button>
<form action="{{ route('categories.destroy',$id) }}" id="form_{{ $id }}" method="post" style="display:inline-block;">
    @csrf
    @method('delete')
</form>
@else
<a href="#" class="btn btn-danger disabled btn-xs"><i class="fa fa-trash"></i> @lang('site.delete')</a>
@endif