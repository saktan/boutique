{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title')
  @lang('site.edit_category')
@stop
@section('content_header')
    <h1>@lang('site.edit_category'):</h1>
    {{ Breadcrumbs::render('edit_category',$category) }}
@stop

@section('content')

<div class="row ">
    <div class="col-md-12">

                <div class="box box-info">

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">

                                <form action="{{ route('categories.update',$category->id) }}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <!--  Name -->

                                    @foreach(config('translatable.locales') as $local)

                                        <div class="form-group @error($local.'.name') has-error @enderror">
                                            <label>@lang('site.'.$local.'.name'):</label>
                                            <input type="text" class="form-control" name="{{ $local }}[name]" value="{{ @$category->translate($local)->name }}">
                                            
                                                <span class="text text-danger">@error($local.'.name')
                                                {{ $message }}
                                                @enderror</span>
                                            
                                        </div>

                                    @endforeach

                                  <!--Submit-->
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">@lang('site.update')</button>
                                    </div>                                
                                </form>

                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->
    </div>
</div>

@stop