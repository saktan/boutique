{{-- resources/views/category/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title')
    @lang('site.categories')
@stop

@section('content_header')
    <h1>@lang('site.categories')</h1>
    {{ Breadcrumbs::render('categories') }}
@stop

@section('content')

<div class="box box-primary">
            <div class="box-header">
            <div class="row">
                <div class="col-md-8">
                    <!-- Create category -->

                    @if(auth::User()->can('create_categories'))
                        <a class="btn btn-primary" href="{{ route('categories.create') }}"><i class="fa fa-plus"></i> @lang('site.create_category')</a>
                        @else
                        <a class="btn btn-primary disabled" href="#"><i class="fa fa-plus"></i> @lang('site.create_category')</a>
                    @endif

                </div>

                <div class="col-md-4">
                    
                </div>
            </div>
            
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-responsive" id="datatable">
              <thead>
                    <tr>
                        <th>#</th>
                        <th>@lang('site.category')</th>
                        <th>@lang('site.actions')</th>
                    </tr>
              </thead>
               
              </table>
            </div>
            <!-- /.box-body -->
          </div>
@stop

@section('js')
<script>

        $('#datatable').DataTable({
            //responsive : true,
            processing : true,
            ajax : '{{ route('categories.index') }}',
            columns : [
                {'data' : 'id'},
                {'data' : 'name'},
                {'data' : 'actions'},
            ],
        });
    </script>   
@stop
