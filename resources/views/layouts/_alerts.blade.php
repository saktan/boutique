<!--
@if($errors->any())
    
    @foreach($errors->all() as $error)
    @endforeach
    
@endif
-->

@php 
    $types = ['alert', 'success', 'error', 'warning', 'info'];
    
@endphp
@foreach($types as $type)

    @if(session($type))
        @section('adminlte_js')
            <script>

                new Noty({
                    text: '{{ session($type) }}',
                    type     : '{{ $type }}',
                }).show();

            </script>
        @endsection
    @endif

@endforeach