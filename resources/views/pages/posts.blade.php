@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <ul class="list-group">
                @forelse($posts as $post)
                    <li class="list-group-item">{{ $post->title }}</li>
                @empty
                    <li class="list-group-item">No Posts !</li>
                @endforelse
            </ul>
            <br/>
            {{ $posts->links() }}
        </div>

    </div>
@stop