 <!-- language selector -->
  <li class="dropdown tasks-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
      <i class="fa fa-flag-o"></i>
      <span class="label label-danger">{{ count(LaravelLocalization::getSupportedLocales()) }}</span>
    </a>
    <ul class="dropdown-menu">
      <li>
        <ul class="menu">
            
            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                <li>
                    <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                        {{ $properties['native'] }}
                    </a>
                </li>
            @endforeach

        </ul>
      </li>
    </ul>
  </li>