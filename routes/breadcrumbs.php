<?php

/*
*
*Laravel Breadcrumbs - A simple Laravel-style way to create breadcrumbs 
*https://github.com/davejamesmiller/laravel-breadcrumbs#getting-started
*
*/

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('Home', route('home.index'));
});

// Home > Users
Breadcrumbs::for('admins', function ($trail) {
    $trail->parent('home');
    $trail->push('Admins', route('admins.index'));
});

Breadcrumbs::for('create_users', function ($trail) {
    $trail->parent('home');
    $trail->push('Admins', route('admins.index'));
    $trail->push('create', route('admins.create'));
});

Breadcrumbs::for('edit_users', function ($trail,$user) {
    $trail->parent('home');
    $trail->push('Admins', route('admins.index'));
    $trail->push('Edit: '.$user->first_name.' '.$user->last_name, route('admins.edit',$user->id));
});

// Home > Categories
Breadcrumbs::for('categories', function ($trail) {
    $trail->parent('home');
    $trail->push('Categories', route('categories.index'));
});

Breadcrumbs::for('create_category', function ($trail) {
    $trail->parent('home');
    $trail->push('Categories', route('categories.index'));
    $trail->push('create', route('categories.create'));
});

Breadcrumbs::for('edit_category', function ($trail,$category) {
    $trail->parent('home');
    $trail->push('Categories', route('categories.index'));
    $trail->push('Edit', route('categories.edit',$category->id));
});