<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ],
    function()
    {
    /** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/
    Auth::routes(['verify' => true]);

    Route::get('/','HomeController@index');

    Route::resource('posts','PostController');

    /** normal user routes **/
    Route::group([
        'middleware'=>['auth','verified']
        ],function(){
        Route::get('/session', 'HomeController@session')->name('session');
        Route::get('/home', 'HomeController@index')->name('home');
    });


    /** admin routes **/
    Route::group(
        [
            'prefix'=>'cpanel',
            'middleware'=>['auth','verified','roles'],
            'roles'=>'admin'
        ],
        function(){
            Route::resource('home','Cpanel\CPHomeController');
            Route::resource('/','Cpanel\CPHomeController');
            Route::resource('admins','Cpanel\CPAdminsController')->except('show');
            Route::resource('categories','Cpanel\CPCategoriesController')->except('show');
            Route::resource('products','Cpanel\CPProductsController')->except('show');
    });
});