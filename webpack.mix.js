const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');



mix.scripts([
   'public/vendor/noty/lib/noty.min.js',
   'public/vendor/mo-js/build/mo.min.js',
   'public/vendor/noty-mo-animation/nma.js'
],'public/js/noty.js');